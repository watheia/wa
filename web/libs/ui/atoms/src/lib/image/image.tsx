import Image, { ImageProps } from 'next/image';
export type { ImageProps } from 'next/image'
export { Image };
export default Image;
